package by.intexsoft.memoryspace.view;

import android.app.Activity;

/**
 * Created by vadim on 07.11.2014.
 */

public interface BaseView
{
    Activity getContainer();
}
