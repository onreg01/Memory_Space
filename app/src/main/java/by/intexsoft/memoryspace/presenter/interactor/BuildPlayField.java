package by.intexsoft.memoryspace.presenter.interactor;

import android.content.Context;
import android.view.ViewGroup;

/**
 * Created by vadim on 07.11.2014.
 */
public interface BuildPlayField
{
    void buildPlayField(Context contexts, ViewGroup viewTop, ViewGroup viewBot);
}
