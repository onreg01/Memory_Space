package by.intexsoft.memoryspace.presenter.interactor;

import android.content.Context;
import android.widget.ImageView;

import by.intexsoft.memoryspace.R;
import by.intexsoft.memoryspace.view.image_view.CustomImageView;

/**
 * Created by vadim on 08.11.2014.
 */

public class BuildPlayFielHorzintalOrientation extends BuildPlayFielVerticalOrientation
{


    public BuildPlayFielHorzintalOrientation(int rows, int column)
    {
        super(rows, column);
    }

    @Override
    public ImageView getImageView(Context context)
    {
        CustomImageView imageView = new CustomImageView(context);
        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.vopros));
        imageView.setBackgroundColor(context.getResources().getColor(R.color.orange));
        return imageView;
    }
}
