package by.intexsoft.memoryspace.ui.activity;

import android.app.Activity;
import android.content.res.Configuration;;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.LinearLayout;

import by.intexsoft.memoryspace.R;
import by.intexsoft.memoryspace.presenter.PlayScreenActivityPresenter;
import by.intexsoft.memoryspace.presenter.PlayScreenActivityPresenterImpl;
import by.intexsoft.memoryspace.presenter.interactor.BuildPlayFielHorzintalOrientation;
import by.intexsoft.memoryspace.presenter.interactor.BuildPlayFielVerticalOrientation;
import by.intexsoft.memoryspace.view.PlayScreenActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;


/**
 * Created by anastasya.konovalova on 04.11.2014.
 */

@EActivity(R.layout.activity_play_screen)
public class PlayScreenActivity extends Activity implements PlayScreenActivityView
{
    private static final String ORIENTATION_VERTICAL = "0";
    private static final String ORIENTATION_HORIZONTAL = "1";

    @Extra
    int rows;

    @Extra
    int column;

    @ViewById
    LinearLayout topLayout;

    @ViewById
    LinearLayout botLayout;

    @Bean(PlayScreenActivityPresenterImpl.class)
    PlayScreenActivityPresenter presenter;

    @AfterViews
    public void init()
    {
        presenter.init(this);
        initPlayField();
    }

    public void initPlayField()
    {
        String orientation = getCurrentOrientation();
        if (orientation.equals(ORIENTATION_VERTICAL))
        {
            presenter.buildPlayField(this, topLayout, botLayout, new BuildPlayFielVerticalOrientation(rows, column));
        }
        else
        {
            int rows = 2;
            int column = (this.rows * this.column) / 2;

            if (rows * column <= 3)
            {
                rows = this.rows;
                column = this.column;
            }
            else if (rows * column >= 30)
            {
                rows = 3;
                column = (this.rows * this.column) / 3;
            }

            presenter.buildPlayField(this, topLayout, botLayout, new BuildPlayFielHorzintalOrientation(rows, column));
        }
    }

    public String getCurrentOrientation()
    {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);

        if (metricsB.widthPixels < metricsB.heightPixels)
        {
            return ORIENTATION_VERTICAL;
        }
        else
        {
            return ORIENTATION_HORIZONTAL;
        }
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        topLayout.removeAllViewsInLayout();
        botLayout.removeAllViewsInLayout();
        initPlayField();
    }
}


